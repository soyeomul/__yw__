# -*- coding: utf-8 -*-

"""
^ARMY_^))// 

방탄소년단(BTS)을 호위하는 *자발적이며 밝음의 포스를 지닌* 최후방 보루이며 
이들의 화합단결력은 하늘에까지 솟구친다... 무려 국제연합(UN) 
[Top Rank Sustainable Global Group]에 등재되어 있다...
"""

탄이들 = (
    "김석진",
    "민윤기",
    "정호석",
    "김남준",
    "박지민",
    "김태형",
    "전정국",
)

__ARMY__ = "^ARMY: Adorable Representative M.C for Youth_^))//"

# 참고문헌:
# [1] https://en.wikipedia.org/wiki/BTS
# [2] https://www.youtube.com/watch?v=ZIgHGISVu_0
# [3] https://www.clien.net/service/board/park/15717764

# 편집: GNU Emacs 27.1 (Ubuntu 18.04)
# 마지막 갱신: 2020년 12월 30일
