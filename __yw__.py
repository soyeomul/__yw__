# -*- coding: utf-8 -*-

###
### __yw__
###

from lex__ import _get_url # custom module
import json

jurl = "https://gitlab.com/soyeomul/__yw__/-/raw/master/__yw__.json"
jdata = _get_url(jurl)

yw = data = json.loads(jdata)

###
### 輪廻 -- Karmic Cycle
###

def karmic_cycle(length):
    from time import time
    """
    random 모듈이 좀 무겁다는 생각에...
    저녁 소여물 주면서 계속 랜덤함수를 생각했어요
    끊임없이 변하는것을 변하지 않는것으로 나누면 좋겠다싶었어요
    변하는건 시간, 변하지않는건 공간 그래서 이 함수가 만들어졌어요

    부처님오신날이라 그런지
     이게 꼭 불교의 윤회(자연의 순환-반복하는 이치)처럼 느껴졌습니다
    --소여물, 2020년 4월 30일 (음력 4월 8일)
    """
    _constant = 108 * 361 * 13 # 시간을 좀 더 세분화시킴
    vtime = int(time() * _constant)

    return vtime % length

###
### 체면장(體面章) -- 공사 제三장 42절
###

체면장 = """\
이해 섣달에 공사를 보실 때 체면장(體面章)을 지으셨도다.
維歲次戊申十二月七日
道術 敢昭告于
惶恐伏地問安 氣體候萬死不忠不孝無序身泣 祝於君於父於師氣候大安千萬
伏望伏望"""

# 편집: GNU Emacs 27.1 (Ubuntu 18.04)
# 마지막 갱신: 2021년 1월 25일
