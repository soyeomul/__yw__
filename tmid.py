#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# 참고문헌: (message-unique-id 함수)
# http://git.savannah.gnu.org/cgit/emacs.git/tree/lisp/gnus/message.el

# Special Message-ID for __yw__

from time import time
from datetime import datetime
from math import pi
import sys

###
###
###
from __yw__ import yw # custom module: __yw__
from __yw__ import karmic_cycle as trandom # custom module: 미륵불
###
###
###

siy = yw["1"] # "yw"
sig = "fsf" # Gnus' Kernel

# __yw__: 절대상수값 미리 정의하기
__const_delta = []
for 종통 in yw["2"]:
    __const_delta.append(yw["2"][종통])
__const_delta = [int(x, 16) for x in __const_delta]
__const_delta = sum(__const_delta) / 2
__const_delta = (pi * __const_delta ** 3) * 3 / 4
__const_delta = int(__const_delta)

# 올해 年號 계산하기
year_now = datetime.now().strftime("%Y")
year_yw = int(year_now) - int(yw["3"][0][:4]) + 1

# 현재 유닉스 시간
time_now = int(time())

###
###
###
from lex__ import _uuid # custom module
###
###
###

uuid = _uuid()

###
###
###
from lex__ import base36 # custom module
###
###
###

rs = "zyxwvutsrqponmlkjihgfedcba9876543210"
karma = list(rs)

YBASE36 = base36(year_yw)
YBASE360 = karma[YBASE36[0]]
YBASE361 = karma[YBASE36[1]]

TBASE36 = base36(time_now)
TBASE360 = karma[TBASE36[0]]
TBASE361 = karma[TBASE36[1]]
TBASE362 = karma[TBASE36[2]]
TBASE363 = karma[TBASE36[3]]
TBASE364 = karma[TBASE36[4]]
TBASE365 = karma[TBASE36[5]]
              
UBASE36 = base36(uuid)
""" Linux/*BSD에서 쓸 수 있는 모든값을 산정했음: 0-65535 """
if len(UBASE36) == 2:
    UBASE360 = karma[UBASE36[0]]
    UBASE361 = karma[UBASE36[1]]
if len(UBASE36) == 3:
    UBASE360 = karma[UBASE36[0]]
    UBASE361 = karma[UBASE36[1]]
    UBASE362 = karma[UBASE36[2]]
if len(UBASE36) == 4:
    UBASE360 = karma[UBASE36[0]]
    UBASE361 = karma[UBASE36[1]]
    UBASE362 = karma[UBASE36[2]]
    UBASE363 = karma[UBASE36[3]]

if len(UBASE36) == 2:
    rns = karma[:10]
if len(UBASE36) == 3:
    rns = karma[:11]
if len(UBASE36) == 4:
    rns = karma[:12]
    
if len(TBASE36) > 6:
    sys.exit("^고맙습니다 감사합니다_^))//") # 후천이기에 의미없음 ;;;
else:
    rns[0] = siy[0]
    rns[1] = siy[1]
    rns[2] = TBASE360
    rns[3] = TBASE361
    rns[4] = TBASE362
    rns[5] = TBASE363
    rns[6] = TBASE364
    rns[7] = TBASE365
    if len(UBASE36) == 2:
        rns[8] = UBASE360
        rns[9] = UBASE361
    if len(UBASE36) == 3:
        rns[8] = UBASE360
        rns[9] = UBASE361
        rns[10] = UBASE362
    if len(UBASE36) == 4:
        rns[8] = UBASE360
        rns[9] = UBASE361
        rns[10] = UBASE362
        rns[11] = UBASE363
        
# 유일한 Message-ID를 위한 난수 조합
rn = "".join(rns) # 매순간 임의로 만들어질 아이디 영역

###
###
###
from lex__ import make_4rd, _delta, three, _28, _24 # custom module
###
###
###

delta = []
for 포덕천하 in _delta:
    delta.append(_delta[포덕천하])

length_delta = len(delta)
length_three = len(three)

delta = delta[trandom(length_delta)]

if delta == "淵源":
    three = three[trandom(length_three)]
    if three == "Σ": # 陰
        trn = ord(_28[trandom(28)])
        trn = __const_delta % trn
    elif three == "Λ": # 陽
        trn = make_4rd(_24[trandom(24)])[1]
        trn = __const_delta % trn
    else: # 道
        trn = ord("Δ")
        trn = __const_delta % trn
    THREE_BASE36 = base36(trn)
    if len(THREE_BASE36) >= 3:
        THREE_BASE360 = karma[THREE_BASE36[0]]
        THREE_BASE361 = karma[THREE_BASE36[1]]
        THREE_BASE362 = karma[THREE_BASE36[2]]
        DELTA = THREE_BASE360 + THREE_BASE361 + THREE_BASE362
    else:
        THREE_BASE360 = karma[THREE_BASE36[0]]
        THREE_BASE361 = karma[THREE_BASE36[1]]
        DELTA = THREE_BASE360 + THREE_BASE361
else:
    dln = make_4rd(delta)[1]
    dln = __const_delta % dln
    DELTA_BASE36 = base36(dln)
    DELTA_BASE360 = karma[DELTA_BASE36[0]]
    DELTA_BASE361 = karma[DELTA_BASE36[1]]
    DELTA_BASE362 = karma[DELTA_BASE36[2]]
    DELTA = DELTA_BASE360 + DELTA_BASE361 + DELTA_BASE362

# 호스트 영역
from 크롬북 import CROS_CODENAME # custom module
hn = "{0}{1}{2}.{3}".format(
    YBASE360,
    YBASE361,
    DELTA,
    CROS_CODENAME[0]
)

# 최종 Message-ID 조합
thanks_mid = "<" + rn + "." + sig + "@" + hn + ">"

def main():
    if len(sys.argv) == 1:
        print(thanks_mid)
    elif sys.argv[1] == "---": # 디버깅
        print(thanks_mid, __const_delta)
    elif sys.argv[1] == "--uuid": # 디버깅
        print(thanks_mid, uuid)
    elif sys.argv[1] == "--whois": # 디버깅
        from lex__ import __whois__ # custom module
        print(thanks_mid, uuid, __whois__[0])
    else:
        print("알수 없는 에러가 발생했습니다.")

if __name__ == "__main__":
    sys.exit(main())
    
# 편집: GNU Emacs 26.3 (Ubuntu 18.04)
# 마지막 갱신: 2021년 1월 28일

"""
# 실험 환경: 우분투 18.04 LTS / 파이썬 3.6.9

(bionic)soyeomul@localhost:~/gitlab/xyz$ ./tmid.py --whois
<yw9ceb3uyne.fsf@vsy4t.oak-birch> 1749 민윤기
(bionic)soyeomul@localhost:~/gitlab/xyz$ ./tmid.py --uuid
<yw9ceb3ppjk.fsf@vsykf.oak-birch> 13551
(bionic)soyeomul@localhost:~/gitlab/xyz$ ./tmid.py
<yw9ceb3hw8k.fsf@vsyd6.oak-birch>
(bionic)soyeomul@localhost:~/gitlab/xyz$ ./tmid.py --
알수 없는 에러가 발생했습니다.
(bionic)soyeomul@localhost:~/gitlab/xyz$ ./tmid.py ---
<yw9ceb34w8k.fsf@vsgzt.oak-birch> 68977084170063502951179336673309721601507328
(bionic)soyeomul@localhost:~/gitlab/xyz$ uname -a
Linux localhost 3.18.0-20580-g316002d07643 #1 SMP PREEMPT Thu Dec 10 19:53:43 PST 2020 aarch64 aarch64 aarch64 GNU/Linux
(bionic)soyeomul@localhost:~/gitlab/xyz$ 
"""
