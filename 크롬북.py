# -*- coding: utf-8 -*-

# 크롬북의 코드명 정보를 기록합니다
# CROS_CODENAME ===> BaseBoard-CodeName

from platform import linux_distribution as _distro

CROS_CODENAME = []

if _distro()[0] == "Debian":
    CROS_CODENAME.insert(0, "zork-morphius")
elif _distro()[0] == "Ubuntu":
    CROS_CODENAME.insert(0, "oak-birch")
else:
    CROS_CODENAME.insert(0, "google")

# 참고문헌: https://www.chromium.org/chromium-os/developer-information-for-chrome-os-devices

# 편집: GNU Emacs 27.1 (Ubuntu 18.04)
# 마지막 갱신: 2021년 1월 5일
