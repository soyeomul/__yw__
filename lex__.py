# -*- coding: utf-8 -*-

#
# Message-ID 생성할때 필요한 도구 모음
#

### base36 (36 진법) ###
# 가령 n = 39 이면
# n = 1 * 36 ** 1 + 3 * 36 ** 0
# 13(36) ===> yw
def base36(n): # 0 <= n < 2176782336 (36 ** 6)
    if n < 0 or n >= 2176782336:
        exit("Sorry it is wrong number, good numbers are [0-2176782335].")
    n1 = n // 36
    if n1 < 36:
        Q0 = n1
        Q1 = n % 36
        return Q0, Q1
    else:
        n2 = n1 // 36
        if n2 < 36:
            Q0 = n2
            Q1 = n1 % 36
            Q2 = n % 36
            return Q0, Q1, Q2
        else:
            n3 = n2 // 36
            if n3 < 36:
                Q0 = n3
                Q1 = n2 % 36
                Q2 = n1 % 36
                Q3 = n % 36
                return Q0, Q1, Q2, Q3
            else:
                n4 = n3 // 36
                if n4 < 36:
                    Q0 = n4
                    Q1 = n3 % 36
                    Q2 = n2 % 36
                    Q3 = n1 % 36
                    Q4 = n % 36
                    return Q0, Q1, Q2, Q3, Q4
                else:
                    n5 = n4 // 36
                    if n5 < 36:
                        Q0 = n5
                        Q1 = n4 % 36
                        Q2 = n3 % 36
                        Q3 = n2 % 36
                        Q4 = n1 % 36
                        Q5 = n % 36
                        return Q0, Q1, Q2, Q3, Q4, Q5

### 파일 내려받기 함수 ###
def _get_url(xyz):
    from subprocess import Popen, PIPE
    """
    `curl' 은 Linux/*BSD 등에서 유명한 도구입니다
    본 코드는 그래서 가급적 Linux/*BSD 시스템에서 실험하시길 권유드립니다
    """
    _cmd = "{0} -s -f {1}".format("curl", xyz)
    _try = Popen(_cmd, stdout=PIPE, shell=True)

    output = _try.communicate()[0].decode("utf-8").strip()

    return output
    
### 문자열을 숫자로 바꾸어 합 구하기 ###
def make_4rd(xyz):
    ls_xyz = []
    for k in xyz:
        ls_xyz.append(ord(k))

    return xyz, sum(ls_xyz)

### 현재 사용자의 UID 정보 ###
__whois__ = []
def _uuid():
    from datetime import datetime
    import os
    
    from __yw__ import yw # custom module
    
    _uuid_today = datetime.now().strftime("%d")
    _getuid = os.getuid()

    _21 = yw["4"]
    _21 = [int(x, 16) for x in _21]
    
    _23 = yw["5"]
    _23 = [int(x, 16) for x in _23]

    _38 = yw["3"]
    _38 = [int(x) for x in _38]

    # ^고맙습니다 _布德天下_ 감사합니다_^))//
    THANKS = (sum(_21) + sum(_23)) * sum(_38)

    if int(_uuid_today) % int(yw["0"]) == 0 and int(_uuid_today) != 24:
        urn = (_getuid + THANKS) % 65536

        __whois__.append("__yw__")

        return urn
    
    elif int(_uuid_today) % len(yw["3"][0]) == 0:
        from __yw__ import 체면장 # custom module
        
        urn = (make_4rd(체면장)[1] + THANKS) % 65536

        __whois__.append("체면장")
        
        return urn
    
    else:
        from __yw__ import karmic_cycle as trandom # custom module
        from army__ import 탄이들 as bts # custom module
        from army__ import __ARMY__ # custom module
               
        bts_member = bts[trandom(len(bts))]
        bts_number = (make_4rd(bts_member)[1] + THANKS) % 65536
        urn = bts_number

        __whois__.append(bts_member)
        
        return urn
         
### 사군자(四君子) ###
# 매: valli = "雪梅"
# 난: orchi = "蘭草"
# 국: chrys = "秋菊"
# 죽: bambu = "靑竹"
_delta = {
    "valli": "雪梅",
    "orchi": "蘭草",
    "chrys": "秋菊",
    "bambu": "靑竹",
    "delta": "淵源",
}

### 델타 델타 델타 !!! ###
# 연원 세분의 탄강지를 선으로 연결 ===> Δ(U+0394)
# x = ord('Δ'); xx = hex(x) 
# y = ord('Λ'); yy = hex(y)
# z = ord('Σ'); zz = hex(z)
# print(x, xx, type(x)) # 916 0x394 <class 'int'> -- aj
# print(y, yy, type(y)) # 923 0x39b <class 'int'> -- ac
# print(z, zz, type(z)) # 931 0x3a3 <class 'int'> -- a4
three = [
    "Σ",
    "Λ",
    "Δ",
]

### 이십팔수주 ###
_28 = (
    "각", "항", "저", "방", "심", "미", "기",
    "두", "우", "여", "허", "위", "실", "벽",
    "규", "루", "위", "모", "필", "자", "삼",
    "정", "귀", "유", "성", "장", "익", "진",
)

### 이십사절후 ###
_24 = (
    "동지", "소한", "대한", "입춘", "우수", "경칩",
    "춘분", "청명", "곡우", "입하", "소만", "망종",
    "하지", "소서", "대서", "입추", "처서", "백로",
    "추분", "한로", "상강", "입동", "소설", "대설",
)

# 편집: GNU Emacs 26.3 (Ubuntu 18.04)
# 마지막 갱신: 2021년 1월 25일
